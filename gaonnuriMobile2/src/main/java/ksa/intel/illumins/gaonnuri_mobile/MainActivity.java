package ksa.intel.illumins.gaonnuri_mobile;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.heinrichreimersoftware.materialdrawer.DrawerFrameLayout;
import com.heinrichreimersoftware.materialdrawer.structure.DrawerItem;
import com.heinrichreimersoftware.materialdrawer.structure.DrawerProfile;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardView;

public class MainActivity extends ActionBarActivity {

    private static Context mContext;
    private static SharedPreferences mSharedPreferences;
    private boolean IS_FIRST_LAUNCH;
    private String LOGIN_ID;
    private String[] mFragmentTitles;
    private DrawerFrameLayout mDrawerFrameLayout;
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private SharedPreferences.Editor mSharedPreferencesEditor;
    private LoginTask mLogin;
    private EditText mEditTextID;
    private EditText mEditTextPW;
    private ArrayList<Fragment> fragmentLists;


    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    /*public static String getHtml(String paramString) throws ClientProtocolException, IOException {
        InputStream localInputStream = new DefaultHttpClient().execute(new HttpGet(paramString)).getEntity().getContent();
        BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localInputStream));
        StringBuilder localStringBuilder = new StringBuilder();
        while (true) {
            String str = localBufferedReader.readLine();
            if (str == null) {
                localInputStream.close();
                return localStringBuilder.toString();
            }
            localStringBuilder.append(str);
        }
    }*/

    public static Context getAppContext() {
        return mContext;
    }

    public static String getDate() {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        TimeZone tz = TimeZone.getTimeZone("Asia/Seoul");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public static String getTodayBreakfast() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("today_breakfast", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);

    }

    public static String getTodayLunch() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("today_lunch", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);
    }

    public static String getTodayDinner() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("today_dinner", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);
    }

    public static String getTodaySnack() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("today_snack", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);
    }

    public static String getTomorrowBreakfast() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("tomorrow_breakfast", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);

    }

    public static String getTomorrowLunch() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("tomorrow_lunch", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);
    }

    public static String getTomorrowDinner() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("tomorrow_dinner", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);
    }

    public static String getTomorrowSnack() {
        if (mSharedPreferences.getString("meal_info_date", "").equals(getDate()))
            return mSharedPreferences.getString("tomorrow_snack", getAppContext().getString(R.string.no_information));
        else
            return getAppContext().getString(R.string.no_information);
    }

    public static ArrayList<CardView> makeMealCardViewArrayList() {

        ArrayList<CardView> array = new ArrayList<CardView>();

        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);

        if (hour < 9)
            array.add(makeMealCardView(true, 0, getTodayBreakfast()));
        if (hour < 13)
            array.add(makeMealCardView(true, 1, getTodayLunch()));
        if (hour < 19)
            array.add(makeMealCardView(true, 2, getTodayDinner()));
        array.add(makeMealCardView(true, 3, getTodaySnack()));
        array.add(makeMealCardView(false, 0, getTomorrowBreakfast()));
        array.add(makeMealCardView(false, 1, getTomorrowLunch()));
        array.add(makeMealCardView(false, 2, getTomorrowDinner()));
        array.add(makeMealCardView(false, 3, getTomorrowSnack()));

        return array;
    }

    public static CardView makeMealCardView(boolean isToday, int mode, String menu) {
        CardView cardView = new CardView(getAppContext());
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        cardView.setLayoutParams(params);
        cardView.setMinimumHeight(0);

        MealCard mealCard = new MealCard(getAppContext(), isToday, mode, menu);

        CardHeader cardHeader = new CardHeader(getAppContext());
        if (isToday) {
            if (mode == 0)
                cardHeader.setTitle(getAppContext().getString(R.string.today) + " " + getAppContext().getString(R.string.breakfast));
            else if (mode == 1)
                cardHeader.setTitle(getAppContext().getString(R.string.today) + " " + getAppContext().getString(R.string.lunch));
            else if (mode == 2)
                cardHeader.setTitle(getAppContext().getString(R.string.today) + " " + getAppContext().getString(R.string.dinner));
            else if (mode == 3)
                cardHeader.setTitle(getAppContext().getString(R.string.today) + " " + getAppContext().getString(R.string.snack));

        } else if (!isToday) {
            if (mode == 0)
                cardHeader.setTitle(getAppContext().getString(R.string.tomorrow) + " " + getAppContext().getString(R.string.breakfast));
            else if (mode == 1)
                cardHeader.setTitle(getAppContext().getString(R.string.tomorrow) + " " + getAppContext().getString(R.string.lunch));
            else if (mode == 2)
                cardHeader.setTitle(getAppContext().getString(R.string.tomorrow) + " " + getAppContext().getString(R.string.dinner));
            else if (mode == 3)
                cardHeader.setTitle(getAppContext().getString(R.string.tomorrow) + " " + getAppContext().getString(R.string.snack));

        }
        mealCard.addCardHeader(cardHeader);
        cardView.setCard(mealCard);

        return cardView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = getApplicationContext();

        mSharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        mSharedPreferencesEditor = mSharedPreferences.edit();

        IS_FIRST_LAUNCH = mSharedPreferences.getBoolean("is_first_launch", true);
        LOGIN_ID = mSharedPreferences.getString("login_id", "");

        // if (IS_FIRST_LAUNCH) {
        Intent intent = new Intent(this, FirstLaunchActivity.class);
        startActivity(intent);
        // }

		/*
         * if (LOGIN_ID == "") { Intent intent = new Intent(this, LoginActivity.class); }
		 */


        mLogin = new LoginTask("id", "pw", MainActivity.this, mContext);
        mLogin.execute((String[]) null);

        mTitle = getResources().getString(R.string.app_name);

        fragmentLists = new ArrayList<Fragment>();
        fragmentLists.add(new HomeFragment());
        fragmentLists.add(new MealInfoFragment());
        fragmentLists.add(new Fragment());
        fragmentLists.add(new Fragment());

        mFragmentTitles = new String[]{getResources().getString(R.string.home), getResources().getString(R.string.meal_info), "three", "four"};
        mDrawerFrameLayout = (DrawerFrameLayout) findViewById(R.id.drawer_layout);
        final MaterialDialog mMaterialDialog = new MaterialDialog.Builder(MainActivity.this).title(R.string.login)
                .customView(R.layout.login_dialog, false).positiveText(R.string.login).negativeText(android.R.string.cancel)
                .callback(new MaterialDialog.ButtonCallback() {

                    @Override
                    public void onPositive(MaterialDialog arg0) {
                        // TODO Auto-generated method stub
                        super.onPositive(arg0);
                        LoginTask mLoginTask = new LoginTask(mEditTextID.getText().toString(), mEditTextPW.getText().toString(), MainActivity.this,
                                getAppContext());
                        try {
                            String result = mLoginTask.execute("").get();
                            Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog arg0) {
                        // TODO Auto-generated method stub

                    }
                }).build();

        mEditTextID = (EditText) mMaterialDialog.getCustomView().findViewById(R.id.editText_id);
        mEditTextPW = (EditText) mMaterialDialog.getCustomView().findViewById(R.id.editText_password);

        Toolbar mToolbar = (Toolbar) mDrawerFrameLayout.findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerFrameLayout.setStatusBarBackgroundColor(Color.RED);

        mDrawerToggle = new ActionBarDrawerToggle(this,/* host Activity */
                mDrawerFrameLayout, mToolbar, /* DrawerLayout object */
        /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open, /* "open drawer" description */
                R.string.drawer_close /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                replaceFragment(mDrawerFrameLayout.getSelectedPosition());
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mTitle);
            }
        };


        buildMaterialDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        replaceFragment(0);
        mDrawerFrameLayout.selectItem(0);

        /*if (getTodayBreakfast().equals(getResources().getString(R.string.no_information))) {
            updateMealInfo();
        }*/

        mSharedPreferencesEditor.putBoolean("meal_info_auto_update", true);


        mSharedPreferencesEditor.putBoolean("is_first_launch", false);

        mSharedPreferencesEditor.commit();

        if (mSharedPreferences.getBoolean("meal_info_auto_update", false)) {
            Intent serviceIntent = new Intent("ksa.intel.illumins.gaonnuri_mobile.MEAL_INFO_UPDATE_SERVICE");
            serviceIntent.setClass(this, MealInfoUpdateService.class);
            startService(serviceIntent);
        }

        if (!mSharedPreferences.getBoolean("meal_info_auto_update", false)) {
            Intent serviceIntent = new Intent("ksa.intel.illumins.gaonnuri_mobile.MEAL_INFO_UPDATE_SERVICE");
            serviceIntent.setClass(this, MealInfoUpdateService.class);
            startService(serviceIntent);
            Toast.makeText(getApplicationContext(), "fasle", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }


    /**
     * Swaps fragments in the main content view
     */
    /*private void selectItem(int position) {

        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mFragmentTitles[position]);
        mDrawerFrameLayout.closeDrawer(mDrawer);

        switch (position) {
            case 0: // fragment1 // use fragment transaction and add the fragment to the container
                replaceFragment(new HomeFragment(), "HomeFragment");
                break;
            case 1:
                replaceFragment(new MealInfoFragment(), "MealInfoFragment");
                break;
            case 2:
                replaceFragment(new Fragment(), "EmptyFragment");
                break;
            case 3:
                replaceFragment(new Fragment(), "EmptyFragment");
                break;
        }

    }*/
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    public void buildMaterialDrawer() {
        Drawable homeIcon;
        homeIcon = getResources().getDrawable(R.drawable.ic_home);

        mDrawerFrameLayout.setDrawerListener(mDrawerToggle);

        final HomeFragment mHomeFragment = new HomeFragment();

        mDrawerFrameLayout.addItem(new DrawerItem()
                        .setImage(homeIcon)
                        .setTextPrimary(getString(R.string.home))
                        .setOnItemClickListener(new DrawerItem.OnItemClickListener() {
                            @Override
                            public void onClick(DrawerItem drawerItem, int i, int i2) {
                                mDrawerFrameLayout.selectItem(0);
                                mDrawerFrameLayout.closeDrawer();

                            }
                        })
        );

        Drawable foodIcon;
        foodIcon = getResources().getDrawable(R.drawable.ic_food);
        final MealInfoFragment mMealInfoFragment = new MealInfoFragment();

        mDrawerFrameLayout.addItem(new DrawerItem()
                        .setImage(foodIcon)
                        .setTextPrimary(getString(R.string.meal_info))
                        .setOnItemClickListener(new DrawerItem.OnItemClickListener() {
                            @Override
                            public void onClick(DrawerItem drawerItem, int i, int i2) {
                                mDrawerFrameLayout.selectItem(1);
                                mDrawerFrameLayout.closeDrawer();
                            }
                        })
        );


        Drawable avatar;
        avatar = getResources().getDrawable(R.drawable.ic_launcher);


        Drawable background;
        background = getResources().getDrawable(R.drawable.profile_background);


        mDrawerFrameLayout.setProfile(new DrawerProfile()
                        .setAvatar(avatar)
                        .setBackground(background)
                        .setName(getString(R.string.dinner))
                        .setDescription(getString(R.string.tomorrow))
        );
    }

    public void updateMealInfo() {
        new MealTask(mSharedPreferencesEditor).start();
    }

    public void replaceFragment(int position) {
        replaceFragment(fragmentLists.get(position));

    }

    public void replaceFragment(Fragment fragment) {
        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class MealInfoFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";
        /**
         * The fragment argument representing the section number for this fragment.
         */
        ArrayList<CardView> array;

        public MealInfoFragment() {

        }

        /**
         * Returns a new instance of this fragment for the given section number.
         */
        public static MealInfoFragment newInstance(int sectionNumber) {
            MealInfoFragment fragment = new MealInfoFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public ArrayList<CardView> getCardViewArrayList() {
            return array;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            ScrollView mScrollView = new ScrollView(getAppContext());
            array = makeMealCardViewArrayList();

            android.app.FragmentManager fragmentManager = getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            // params.setMargins(0, 0, 0, (int) dipToPixels(getAppContext(), 10));
            mScrollView.setLayoutParams(params);
            LinearLayout linear = new LinearLayout(getAppContext());
            linear.setOrientation(LinearLayout.VERTICAL);
            LayoutParams params1 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            linear.setLayoutParams(params1);
            for (int i = 0; i < array.size(); i++) {
                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                // params2.setMargins((int) dipToPixels(mContext, 10), (int) dipToPixels(mContext, 10), (int) dipToPixels(mContext, 10), 0);
                array.get(i).setLayoutParams(params2);
                linear.addView(array.get(i));
            }
            mScrollView.addView(linear);

            return mScrollView;

        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            // ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();

        }
    }

    public static class HomeFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        CardView nowMealCardView;

        public HomeFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section number.
         */
        public static HomeFragment newInstance(int sectionNumber) {
            HomeFragment fragment = new HomeFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);

            if (hour < 9)
                nowMealCardView = makeMealCardView(true, 0, getTodayBreakfast());
            else if (hour < 13)
                nowMealCardView = makeMealCardView(true, 1, getTodayLunch());
            else if (hour < 19)
                nowMealCardView = makeMealCardView(true, 2, getTodayDinner());
            else if (hour < 22)
                nowMealCardView = makeMealCardView(true, 3, getTodaySnack());
            else
                nowMealCardView = makeMealCardView(false, 0, getTomorrowBreakfast());

            ScrollView mScrollView = new ScrollView(getAppContext());
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            // params.setMargins(0, 0, 0, (int) dipToPixels(getAppContext(), 10));
            mScrollView.setLayoutParams(params);
            LinearLayout linear = new LinearLayout(getAppContext());
            linear.setOrientation(LinearLayout.VERTICAL);
            LayoutParams params1 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            linear.setLayoutParams(params1);
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            // params2.setMargins((int) dipToPixels(mContext, 10), (int) dipToPixels(mContext, 10), (int) dipToPixels(mContext, 10), 0);
            nowMealCardView.setLayoutParams(params2);
            linear.addView(nowMealCardView);

            mScrollView.addView(linear);

            return mScrollView;

        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            // ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();

        }

        public void refresh() {

        }
    }

    /*private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // TODO Auto-generated method stub
            (position);
        }
    }*/

    /*class MealParser extends Thread {

        String todayBreakfast = new String();
        String todayLunch = new String();
        String todayDinner = new String();
        String todaySnack = new String();
        String tomorrowBreakfast = new String();
        String tomorrowLunch = new String();
        String tomorrowDinner = new String();
        String tomorrowSnack = new String();
        ArrayList<String> htmlTokens;
        ArrayList<Integer> nums;

        public MealParser() {

        }

        public String makeMealText(int n) {
            String string = "";
            if (n < 8) {
                for (int i = 0; i < nums.get(n + 1) - nums.get(n) - 1; i++) {
                    string += htmlTokens.get(nums.get(n) + i + 1);
                    string += "\u00A0";
                }
            } else if (n == 8) {
                for (int i = 0; i < htmlTokens.size() - nums.get(8) - 1; i++) {
                    string += htmlTokens.get(nums.get(8) + i + 1);
                    string += "\u00A0";
                }
            }
            return string;
        }

        public void run() {
            htmlTokens = new ArrayList<String>();
            try {
                String url = getHtml("http://gaonnuri.ksain.net/xe/?mid=foodpage");
                if (url != null) {
                    final StringTokenizer localStringTokenizer = new StringTokenizer(Jsoup.parse(url).select("table.foodtable").get(0).text());
                    do {
                        if (!localStringTokenizer.hasMoreTokens())
                            continue;
                        htmlTokens.add(localStringTokenizer.nextToken());
                    } while (localStringTokenizer.hasMoreTokens());

                }


                nums = new ArrayList<Integer>();
                for (int i = 0; i < htmlTokens.size(); i++) {
                    if (htmlTokens.get(i).equals("아침"))
                        nums.add(i);
                    if (htmlTokens.get(i).equals("점심"))
                        nums.add(i);
                    if (htmlTokens.get(i).equals("저녁"))
                        nums.add(i);
                    if (htmlTokens.get(i).equals("간식"))
                        nums.add(i);
                    if (htmlTokens.get(i).equals("내일"))
                        nums.add(i);
                }


                todayBreakfast = makeMealText(0);
                todayLunch = makeMealText(1);
                todayDinner = makeMealText(2);
                todaySnack = makeMealText(3);
                tomorrowBreakfast = makeMealText(5);
                tomorrowLunch = makeMealText(6);
                tomorrowDinner = makeMealText(7);
                tomorrowSnack = makeMealText(8);

                // Log.e("LocationDB",String.format("Here is Call to %s : %s /%s",strWhere,tz.getDisplayName(),df.format(date)));

                mSharedPreferencesEditor.putString("meal_info_date", getDate());
                mSharedPreferencesEditor.putString("today_breakfast", todayBreakfast);
                mSharedPreferencesEditor.putString("today_lunch", todayLunch);
                mSharedPreferencesEditor.putString("today_dinner", todayDinner);
                mSharedPreferencesEditor.putString("today_snack", todaySnack);
                mSharedPreferencesEditor.putString("tomorrow_breakfast", tomorrowBreakfast);
                mSharedPreferencesEditor.putString("tomorrow_lunch", tomorrowLunch);
                mSharedPreferencesEditor.putString("tomorrow_dinner", tomorrowDinner);
                mSharedPreferencesEditor.putString("tomorrow_snack", tomorrowSnack);
                mSharedPreferencesEditor.commit();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        }
    }*/

}