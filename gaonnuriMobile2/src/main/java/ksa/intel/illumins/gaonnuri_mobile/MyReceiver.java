package ksa.intel.illumins.gaonnuri_mobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyReceiver extends BroadcastReceiver {
    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") && (context.getSharedPreferences("preferences", Context.MODE_PRIVATE).getBoolean("meal_info_auto_update", false))) {
            Intent myIntent = new Intent(context, MealInfoUpdateService.class);
            context.startService(myIntent);
        }
    }
}
