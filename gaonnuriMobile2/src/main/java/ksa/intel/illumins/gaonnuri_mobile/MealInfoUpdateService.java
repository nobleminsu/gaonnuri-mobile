package ksa.intel.illumins.gaonnuri_mobile;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class MealInfoUpdateService extends Service implements Runnable {

    private final IBinder binder = new ServiceBinder();
    private int SMALL_DELAY_TIME = 1000 * 60;
    private boolean mIsRunning;
    private Handler mHandler;

    public MealInfoUpdateService() {
    }

    public static String getDate() {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        TimeZone tz = TimeZone.getTimeZone("Asia/Seoul");
        df.setTimeZone(tz);
        return df.format(date);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
// 다른 컴포넌트가 startService()를 호출해서 서비스가 시작되면 이 메소드가 호출됩니다. 만약 연결된 타입의 서비스를 구현한다면 이 메소드는 재정의 할 필요가 없습니다.
        mHandler.postDelayed(this, SMALL_DELAY_TIME);
        mIsRunning = true;

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return binder;
    }

    @Override
    public void onCreate() {
// 서비스가 처음으로 생성되면 호출됩니다. 이 메소드 안에서 초기의 설정 작업을 하면되고 서비스가 이미 실행중이면 이 메소드는 호출되지 않습니다.
        mHandler = new Handler();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void run() {

        if (!mIsRunning ||
                (!getSharedPreferences("preferences", MODE_PRIVATE).getBoolean("meal_info_auto_update", false))) {
            stopSelf();
            Toast.makeText(getApplicationContext(), "Meal Information Auto Update Service Stopped", Toast.LENGTH_SHORT).show();

        } else {
            if (!getDate().equals(getSharedPreferences("preferences", MODE_PRIVATE).getString("meal_info_date", ""))) {
                new MealTask(getSharedPreferences("preferences", MODE_PRIVATE).edit()).start();
                mHandler.postDelayed(this, SMALL_DELAY_TIME);
                mIsRunning = true;
            } else {
                mHandler.postDelayed(this, SMALL_DELAY_TIME);
                mIsRunning = true;
            }
        }
    }

    public class ServiceBinder extends Binder {

        public MealInfoUpdateService getService() {

            return MealInfoUpdateService.this;
        }
    }


}
