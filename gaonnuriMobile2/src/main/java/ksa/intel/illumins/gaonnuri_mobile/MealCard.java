package ksa.intel.illumins.gaonnuri_mobile;

import it.gmariotti.cardslib.library.internal.Card;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * TODO: document your custom view class.
 */
public class MealCard extends Card {
	int BREAKFAST = 0;
	int LUNCH = 1;
	int DINNER = 2;
	int SNACK = 3;
	boolean today;
	int mode;
	String menu;

	public MealCard(Context context) {
		super(context, R.layout.meal_card_inner_layout);
		// TODO Auto-generated constructor stub
	}

	public MealCard(Context context, boolean today_, int mode_, String menu_) {
		this(context);
		today = today_;
		mode = mode_;
		menu = menu_;
		init();
	}

	private void init() {

	}

	@Override
	public void setupInnerViewElements(ViewGroup parent, View view) {
		// Retrieve elements
		TextView mealText = (TextView) parent.findViewById(R.id.meal_text);
		mealText.setText(menu);

	}


}
