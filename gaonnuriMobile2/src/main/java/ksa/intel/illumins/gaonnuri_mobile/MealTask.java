package ksa.intel.illumins.gaonnuri_mobile;

import android.content.SharedPreferences;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

public class MealTask extends Thread {

    String todayBreakfast = new String();
    String todayLunch = new String();
    String todayDinner = new String();
    String todaySnack = new String();
    String tomorrowBreakfast = new String();
    String tomorrowLunch = new String();
    String tomorrowDinner = new String();
    String tomorrowSnack = new String();
    ArrayList<String> htmlTokens;
    ArrayList<Integer> nums;
    SharedPreferences.Editor mSharedPreferencesEditor;

    public MealTask() {

    }

    public MealTask(SharedPreferences.Editor mSharedPreferencesEditor) {
        this.mSharedPreferencesEditor = mSharedPreferencesEditor;
    }

    public static String getHtml(String paramString) throws ClientProtocolException, IOException {
        InputStream localInputStream = new DefaultHttpClient().execute(new HttpGet(paramString)).getEntity().getContent();
        BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localInputStream));
        StringBuilder localStringBuilder = new StringBuilder();
        while (true) {
            String str = localBufferedReader.readLine();
            if (str == null) {
                localInputStream.close();
                return localStringBuilder.toString();
            }
            localStringBuilder.append(str);
        }
    }

    public static String getDate() {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        TimeZone tz = TimeZone.getTimeZone("Asia/Seoul");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public String makeMealText(int n) {
        String string = "";
        if (n < 8) {
            for (int i = 0; i < nums.get(n + 1) - nums.get(n) - 1; i++) {
                string += htmlTokens.get(nums.get(n) + i + 1);
                string += "\u00A0";
            }
        } else if (n == 8) {
            for (int i = 0; i < htmlTokens.size() - nums.get(8) - 1; i++) {
                string += htmlTokens.get(nums.get(8) + i + 1);
                string += "\u00A0";
            }
        }
        return string;
    }

    public void run() {
        htmlTokens = new ArrayList<String>();
        try {
            String url = getHtml("http://gaonnuri.ksain.net/xe/?mid=foodpage");
            if (url != null) {
                final StringTokenizer localStringTokenizer = new StringTokenizer(Jsoup.parse(url).select("table.foodtable").get(0).text());
                do {
                    if (!localStringTokenizer.hasMoreTokens())
                        continue;
                    htmlTokens.add(localStringTokenizer.nextToken());
                } while (localStringTokenizer.hasMoreTokens());

            }

				/*
                 * runOnUiThread(new Runnable() { // development only phrase
				 *
				 * @Override public void run() { // TODO Auto-generated method stub Toast.makeText(getApplicationContext(), htmlTokens.get(20),
				 * 0).show(); }
				 *
				 * });
				 */

            nums = new ArrayList<Integer>();
            for (int i = 0; i < htmlTokens.size(); i++) {
                if (htmlTokens.get(i).equals("아침"))
                    nums.add(i);
                if (htmlTokens.get(i).equals("점심"))
                    nums.add(i);
                if (htmlTokens.get(i).equals("저녁"))
                    nums.add(i);
                if (htmlTokens.get(i).equals("간식"))
                    nums.add(i);
                if (htmlTokens.get(i).equals("내일"))
                    nums.add(i);
            }
                /*
                 * runOnUiThread(new Runnable() { // development only phrase
				 *
				 * @Override public void run() { // TODO Auto-generated method stub
				 *
				 * Toast.makeText(getApplicationContext(), "Padding : " + Integer.toString(width - 16 * dpi / 160 * 2), 0).show();
				 * Toast.makeText(getApplicationContext(), nums.get(2) + " " + nums.get(3), 0).show(); }
				 *
				 * });
				 */

            todayBreakfast = makeMealText(0);
            todayLunch = makeMealText(1);
            todayDinner = makeMealText(2);
            todaySnack = makeMealText(3);
            tomorrowBreakfast = makeMealText(5);
            tomorrowLunch = makeMealText(6);
            tomorrowDinner = makeMealText(7);
            tomorrowSnack = makeMealText(8);

            // Log.e("LocationDB",String.format("Here is Call to %s : %s /%s",strWhere,tz.getDisplayName(),df.format(date)));

            mSharedPreferencesEditor.putString("meal_info_date", getDate());
            mSharedPreferencesEditor.putString("today_breakfast", todayBreakfast);
            mSharedPreferencesEditor.putString("today_lunch", todayLunch);
            mSharedPreferencesEditor.putString("today_dinner", todayDinner);
            mSharedPreferencesEditor.putString("today_snack", todaySnack);
            mSharedPreferencesEditor.putString("tomorrow_breakfast", tomorrowBreakfast);
            mSharedPreferencesEditor.putString("tomorrow_lunch", tomorrowLunch);
            mSharedPreferencesEditor.putString("tomorrow_dinner", tomorrowDinner);
            mSharedPreferencesEditor.putString("tomorrow_snack", tomorrowSnack);
            mSharedPreferencesEditor.commit();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }
}
